process CONCAT {
    tag "$meta.id"

    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://containers.biocontainers.pro/s3/SingImgsRepo/biocontainers/v1.2.0_cv1/biocontainers_v1.2.0_cv1.img' :
        'biocontainers/biocontainers:v1.2.0_cv1' }"

    input:
    tuple val(meta), path(bed_files)

    output:
    tuple val(meta), path("${meta.id}.bed.gz"), emit: concatenated_bed_files 
    script:
    """
    bed_files="$bed_files"
    cat $bed_files > ${meta.id}.unsorted.bed
    if [[ "\$bed_files" =~ .*".gz".* ]]
    then
        zcat ${meta.id}.unsorted.bed | LC_ALL=C sort --parallel=1 -k1,1 -k 2,2n  > ${meta.id}.bed
    else
        cat ${meta.id}.unsorted.bed | LC_ALL=C sort --parallel=1 -k1,1 -k 2,2n  > ${meta.id}.bed
    fi

    if [[ ! \$(head -n 1 ${meta.id}.bed | grep -P "^chr") ]]
    then
        sed -i 's/^/chr/' ${meta.id}.bed
    fi

    rm ${meta.id}.unsorted.bed
    gzip ${meta.id}.bed
    """
}