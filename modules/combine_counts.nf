process COMBINE_COUNTS {
    tag "$meta.id"

    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/python:3.9--1' :
        'quay.io/biocontainers/python:3.9--1' }"

    input:
    tuple val(meta), path(per_strand_bed)
    val(breakcount_threshold)

    output:
    tuple val(meta), path("${meta.id}.combined_counts.gz"),            emit: combined_counts
    tuple val(meta), path("${meta.id}.unfiltered.combined_counts.gz"), emit: unfiltered_combined_counts

    script:
    """
    ls -l
    echo "------------------------------------------------"
    LAST_PATH_ITEM=\${PATH##*:}
    PARENT_DIR=\${LAST_PATH_ITEM%/*}
    for F in \$(find \${PARENT_DIR} -type l -name "*bin*")
    do
        DEST_FILE=\$(readlink -f \$F)
        if [ -L "\$F" ]
        then  # Check if it's a symbolic link
            echo "ls"
            ls -l \$F
            ls -lL \$F

            if [ ! -e \$DEST_FILE ]; then  # Check if the file it points to exists
                echo -e "\$F POINTS TO \$DEST_FILE \\n The symbolic link is broken."
            else
                echo "The symbolic link is not broken."
            fi
        else
            echo "This is not a symbolic link."
        fi
        echo
    done

    
    combine_plus_minus_counts.py \\
    --input ${meta.id}.per_strand.bed.gz \\
    --output ${meta.id}.combined_counts \\
    --break_frequency_output ${meta.id}.frequencies.tsv \\
    --breakcount_threshold $breakcount_threshold

    gzip ${meta.id}.combined_counts
    gzip ${meta.id}.unfiltered.combined_counts
    """
}