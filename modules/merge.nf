process MERGE {
    tag "$meta.id"

    container "${ workflow.containerEngine == 'singularity' && !task.ext.singularity_pull_docker_container ?
        'https://depot.galaxyproject.org/singularity/bedtools:2.30.0--hc088bd4_0' :
        'quay.io/biocontainers/bedtools:2.30.0--hc088bd4_0' }"

    input:
    tuple val(meta), path(concatenated_bed)
    val(merge_distance)

    output:
    tuple val(meta), path("${meta.id}.per_strand.bed.gz"),  emit: per_strand_bed

    script:
    """
    bedtools merge -s -c 2,6 -o count,distinct -d $merge_distance -i $concatenated_bed | \\
    awk -F'\t' 'BEGIN {OFS = FS}{print \$1, \$2, \$3,  \$3"_"\$4\$5}' | \\
    bedtools merge -c 4 -o distinct -i - > ${meta.id}.per_strand.bed
    gzip ${meta.id}.per_strand.bed
    """
}