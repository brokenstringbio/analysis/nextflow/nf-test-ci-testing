//
// Check input samplesheet and get read channels
//

include { SAMPLESHEET_CHECK } from '../../modules/samplesheet_check'

workflow INPUT_CHECK {
    take:
    samplesheet // file: /path/to/samplesheet.csv

    main:
    // check sample sheet is valid
    valid_samplesheet = SAMPLESHEET_CHECK ( samplesheet )
    .csv
    .splitCsv ( header:true, sep:',' )

    valid_samplesheet
    .map { create_data_channel(it) }
    .set { data_files }

    emit: 
        data_files    // channel: [ val(id), [path(breakend_file1), path(breakend_file2),...], val(control), val(guide) ]
    
    versions = SAMPLESHEET_CHECK.out.versions // channel: [ versions.yml ]
}

// function to get meta map from row
def create_meta_map(LinkedHashMap row){
    def meta = [:]
    meta["id"] = row.sample
    return meta
}

// Function to get list of [ sample, bedfile1,bedfile2... ]
def create_data_channel(LinkedHashMap row) {
    def data_filepath_array = []
    def data_filepaths = []
    data_filepaths = row.data_files.split(" ")
    for(filepath in data_filepaths){
        if (!file(filepath).exists()) {
            exit 1, "ERROR: Please check input samplesheet -> Breakend file does not exist!\n${filepath}"
        }
        data_filepath_array.add(file(filepath))
    }
    meta = create_meta_map(row)
    array = [meta, data_filepath_array]
    return array
}

