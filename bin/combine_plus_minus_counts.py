#!/usr/bin/env python 
import csv
import os
import sys
import argparse
import gzip

def parse_args(args=None):
    Description = "Reformat the pre_breakcount file and take the top n lines, or breaks of n frequency and output breakcount frequencies"
    Epilog = "Example usage: python check_samplesheet.py <input> <output> <break_frequency=2>"

    parser = argparse.ArgumentParser(description=Description, epilog=Epilog)
    parser.add_argument("--input", help="the input, which is the pre-breakcount file in the format chr start end 13+/6-")
    parser.add_argument("--output", help="the output file")
    parser.add_argument("--break_frequency_output", help="the break frequency output file")
    parser.add_argument("--breakcount_threshold", help="break frequency we want, e.g. we want breaks of 2 or greater", default = 2, type=int)

    return parser.parse_args(args)


def extract_plus_minus_for_merged(input, output, break_frequency_output, breakcount_threshold):
    recurrent_break_frequencies: dict[int,int] = {}
    unfiltered_output = os.path.splitext(output)[0] + ".unfiltered" + os.path.splitext(output)[1]
    with gzip.open(input, mode = 'rt') as tsv_file, open(output, "w") as outfile, open(unfiltered_output, "w") as unfiltered_outfile:
        reader = csv.reader(tsv_file, delimiter = "\t")
        for row in (reader):
            # format is "31628101_2-,31628102_1+,31628103_1-,31628104_1+"
            strand_count = row[3]
            # this becomes ['31628101_2-', '31628102_1+', '31628103_1-', '31628104_1+']
            pos_and_counts = strand_count.split(",")
            # this becomes ['2-', '1+', '1-', '1+']
            counts = [x.split("_")[-1] for x in pos_and_counts]
            # empty dictionary to collect countd
            count_dict = {"+": 0, "-": 0}
            # add each observation into the dictionary
            for count in counts:
                count_dict[count[-1]] += int(count[0:-1])
            
            # add observation of total count to tally for that value in recurrent_break_frequencies
            total_count = count_dict["+"] + count_dict["-"]
            if total_count not in recurrent_break_frequencies:
                recurrent_break_frequencies[total_count] = 0
            recurrent_break_frequencies[total_count] += 1
            
            items_to_print = [str(x) for x in [row[0],row[1],row[2], count_dict["+"], count_dict["-"],total_count]]
            if total_count >= breakcount_threshold:
                outfile.write("\t".join(items_to_print)+"\n")
            unfiltered_outfile.write("\t".join(items_to_print)+"\n")
    
    with open(break_frequency_output, 'w') as tsv_file:
        for break_frequency in sorted(recurrent_break_frequencies.keys()):
            tsv_file.write(f"{break_frequency}\t{recurrent_break_frequencies[break_frequency]}\n")

def main(args=None):
    args = parse_args(args)
    extract_plus_minus_for_merged(args.input, args.output, args.break_frequency_output, args.breakcount_threshold)

if __name__ == "__main__":
    sys.exit(main())
