#!/usr/bin/env python

import os
import sys
import errno
import argparse


def parse_args(args=None):
    Description = "Reformat samplesheet file and check its contents."
    Epilog = "Example usage: python check_samplesheet.py <FILE_IN> <FILE_OUT>"

    parser = argparse.ArgumentParser(description=Description, epilog=Epilog)
    parser.add_argument("FILE_IN", help="Input samplesheet file.")
    parser.add_argument("FILE_OUT", help="Output file.")
    
    return parser.parse_args(args)


def make_dir(path):
    if len(path) > 0:
        try:
            os.makedirs(path)
        except OSError as exception:
            if exception.errno != errno.EEXIST:
                raise exception

def print_error(error, context="Line", context_str=""):
    error_str = "ERROR: Please check samplesheet -> {}".format(error)
    if context != "" and context_str != "":
        error_str = "ERROR: Please check samplesheet -> {}\n{}: '{}'".format(
            error, context.strip(), context_str.strip()
        )
    print(error_str)
    sys.exit(1)

def check_header(header:list[str]):
    ## Check header
    valid_headers = [
        ["sample", "data_file"],
    ]
    
    if header in valid_headers:
        return True
    else:
        print(f'ERROR: Please check samplesheet header -> {",".join(header)}. Not one of')
        for valid_header in valid_headers:
            print(",".join(valid_header))
        return False


def check_all_values_equal(line, sample, column, sample_mapping_dict, sample_metadata):
    if sample_mapping_dict[sample][column] and sample_mapping_dict[sample][column] != sample_metadata[column]:
        print_error(
            f"Sample {sample} has a different {column} {sample_metadata[column]} compared to other samples in its group \
                {sample_mapping_dict[sample][column]}! \nLine: {line}")
    return True

def check_samplesheet(file_in, file_out):
    """
    This function checks that the samplesheet follows the following structure:
    sample,data_file
    SAMPLE_1,SAMPLE_1_rep1.bed
    SAMPLE_1,SAMPLE_1_rep2.bed
    SAMPLE_2,SAMPLE_2_rep1.bed
    SAMPLE_2,SAMPLE_2_rep2.bed
    
    """

    sample_mapping_dict = {}
    with open(file_in, "r", encoding='utf-8-sig') as input_csv:

        min_cols = 2
        header = [x.strip('"') for x in input_csv.readline().strip().split(",")]
        if not(check_header(header)):
            sys.exit(1)
            
        ## Check sample entries
        for line in input_csv:
            sample_metadata_list: list[str] = [x.strip().strip('"') for x in line.strip().split(",")]

            # Check valid number of columns per row
            if len(sample_metadata_list) < len(header):
                print_error(f"Invalid number of columns (minimum = {len(header)})! \nLine: {line}")
            num_populated_cols = len([x for x in sample_metadata_list if x])
            if num_populated_cols < min_cols:
                print_error(f"Invalid number of populated columns (minimum = {min_cols})!\nLine: {line}")

            ## Check sample name entries
            sample_metadata: dict[str,str] = {}
            for index, column in enumerate(header):
                sample_metadata[column] = sample_metadata_list[index]
 
            # check sample name
            if not sample_metadata['sample']:
                print_error("Sample entry has not been specified!", "Line", line)
            if sample_metadata['sample'].find(" ") != -1:
                print(f"WARNING: Spaces have been replaced by underscores for sample: {sample_metadata['sample']}")
                sample_metadata['sample'] = sample_metadata['sample'].replace(" ", "_")

            ## Check data file extension
            if sample_metadata['data_file'].find(" ") != -1:
                print_error(f"Data file contains spaces!\nLine: {line}")
            if not sample_metadata['data_file'].endswith(".bed") and not sample_metadata['data_file'].endswith(".bed.gz"):
                print_error(f"Data file does not have an extension '.bed' or '.bed.gz' are you sure it's a bed file?\nLine: {line}")
            
            ## Create sample mapping dictionary {sample: data_files, control, guide}
            sample = sample_metadata['sample'] 
            if sample not in sample_mapping_dict:
                sample_mapping_dict[sample] = {'data_files': [], 'control': None}
                for column in [column for column in header if column not in ['sample', 'breakend_files', 'control']]:
                    if column in sample_metadata:
                        sample_mapping_dict[sample][column] = None

            sample_mapping_dict[sample]['data_files'].append(sample_metadata['data_file'])
            
            # check combined sample info 
            for column in ['control']:
                if column in sample_metadata:
                    if check_all_values_equal(line, sample, column, sample_mapping_dict,sample_metadata):
                        sample_mapping_dict[sample][column] = sample_metadata[column]
                    
    ## Write validated samplesheet with appropriate columns
    if len(sample_mapping_dict) > 0:
        out_dir = os.path.dirname(file_out)
        make_dir(out_dir)
        with open(file_out, "w") as output_csv:
            headers = ["sample", "data_files"]

            output_csv.write(",".join(headers) + "\n")
            for sample in sorted(sample_mapping_dict.keys()):
                sample_metadata_to_write = [sample,' '.join(sample_mapping_dict[sample]['data_files'])]
                output_csv.write(",".join(sample_metadata_to_write) + "\n")
    else:
        print_error(f"No samples to process!\nSamplesheet: {file_in}")

def main(args=None):
    args = parse_args(args)
    check_samplesheet(args.FILE_IN, args.FILE_OUT)

if __name__ == "__main__":
    sys.exit(main())
