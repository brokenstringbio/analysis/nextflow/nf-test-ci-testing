class ParamsCheck {
    // Check mandatory input and output parameters
    static def check_input_and_ouput(Map params){
        if (params.samplesheet) {
            return params.samplesheet
        } else { 
            println 'Input samplesheet not specified with --samplesheet or in a config file!'
            System.exit(1)
        }
        if (!params.outdir) {
            println 'Output directory not specified with --outdir or in a config file!'
            System.exit(1)
        }

    }

    static def check_mandatory_params(Map params){
        if (!params.method) {
            println 'Analytical method not specified! --method or in a config file'
            System.exit(1)
        }
        if (!params.platform) {
            println 'Sequencing platform not specified using --platform or in a config file!'
            System.exit(1)
        }
        if (!params.mapq_threshold) {
            println 'MAPQ threshold not specified using --mapq_threshold or in a config file!'
            System.exit(1)
        }
    }

    static def create_index_channel(Map params){
        if (!params.bwa_index_directory && !params.genome) {
            println 'Specify one of bwa_index_directory or genome' 
            System.exit(1)
        } else {
            if (params.bwa_index_directory) {
                return params.bwa_index_directory
            } else if (params.genome){
                if (params.genome == 'hg19') {
                    return 's3://broken-string-biosciences/accessory_files/bwa_indices/hg19.chr/indices'
                } else if (params.genome == 't2t'){
                    return 's3://broken-string-biosciences/accessory_files/bwa_indices/t2t/indices'
                } else {
                    println "Unrecognised genome ${params.genome}" 
                    System.exit(1)
                }
            }
        }
    }

    static def create_refseq_channel(Map params){
        if (params.refseq || params.genome) {
            if (params.refseq){
                return params.refseq
            } else {
                if (params.genome == 'hg19') {
                    return 's3://broken-string-biosciences/accessory_files/bwa_indices/hg19.chr/refseq/hg19.fasta'
                } else if (params.genome == 't2t'){
                    return 's3://broken-string-biosciences/accessory_files/bwa_indices/t2t/refseq/t2t.fasta'
                } else {
                    println "Unrecognised genome ${params.genome}" 
                    System.exit(1)
                }
            }
        } else {
            println 'Reference sequence or genome not specified with --genome, --refseq or in a config file!'
            System.exit(1)
        }
    }

    static def create_chrom_sizes_channel(Map params, String baseDir){
        if (params.chrom_sizes || params.genome){
            if (params.chrom_sizes){
                return params.chrom_sizes
            } else {
                if (params.genome == 'hg19') {
                    return "${baseDir}/assets/bedfiles/hg19.chrom.sizes.bed"
                } else if (params.genome == 't2t') {
                    return "${baseDir}/assets/bedfiles/t2t.chrom.sizes.without_M.bed"
                } else {
                    println "Unrecognised genome ${params.genome}" 
                    System.exit(1)
                }
            }
        } else {
            println 'Specify one of chrom_sizes or genome' 
            System.exit(1)
        }
    }

    static def create_chrom_ends_channel(Map params, String baseDir){
        if (params.chrom_ends || params.genome){
            if (params.chrom_sizes){
                return  params.chrom_ends
            } else {
                if (params.genome == 'hg19') {
                    return  "${baseDir}/assets/bedfiles/hg19.chrom.ends.bed"
                } else if (params.genome == 't2t') {
                    return  "${baseDir}/assets/bedfiles/t2t.dummy.chrom.ends.bed"
                } else {
                    println "Unrecognised genome ${params.genome}" 
                    System.exit(1)
                }
            }
        } else {
            println 'Specify one of chrom_ends or genome' 
            System.exit(1)
        }
    }

    static def create_excludelist_channel(Map params, String baseDir){
        if (params.excludelist || params.genome){
            if (params.excludelist){
                return params.excludelist
            } else {
                if (params.genome == 'hg19') {
                    return "${baseDir}/assets/bedfiles/hg19.excludelist.bed"
                } else if (params.genome == 't2t') {
                    return "${baseDir}/assets/bedfiles/t2t.dummy.excludelist.bed"
                } else {
                    println "Unrecognised genome ${params.genome}" 
            System.exit(1)
                }
            }
        } else {
            println 'Specify one of excludelist or genome' 
            System.exit(1)
        }
    }

    static def check_hla(Map params){
        if (params.hla){
            if (!params.hla_to_t2t_lookup){
                println "Specify a s3 path to the engineered HLA positions converted to T2T .tsv file with --hla_to_t2t_lookup or in a config file!"
                System.exit(1)
            }
            else{
                return params.hla_to_t2t_lookup
            }
        } 
    }
}