// enable DSL2
nextflow.enable.dsl=2

// Import Subworkflows
include { INPUT_CHECK    } from './subworkflows/input_check/input_check'

// Import single processes
include { CONCAT          } from './modules/concat'
include { MERGE          } from './modules/merge'
include { COMBINE_COUNTS } from './modules/combine_counts'

ch_samplesheet = file(ParamsCheck.check_input_and_ouput(params), checkIfExists: true)

workflow combine_counts {
    // meta is a map with keys id , sample_type, control, guide
    // ======== check sample ======== \\
    INPUT_CHECK(ch_samplesheet)
    CONCAT(INPUT_CHECK.out.data_files)
    MERGE(CONCAT.out.concatenated_bed_files, params.merge_distance)
    COMBINE_COUNTS(MERGE.out.per_strand_bed, params.breakcount_threshold)
}

workflow {
    combine_counts()
}

